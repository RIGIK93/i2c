import driver
import RPi.GPIO as GPIO
import random
import time

random.seed(time.time())
driver.ADDRESS = 0x3F
lcd = driver.lcd()
splashes = [lambda: f" days left until the New Year!", "Santa eats nut free cookies only",
"Grinch was here. Hehe.", "Do you prefer presents in socks?", "Coooooooookies!\n*OM.. NOM.. NOM*",
"What exactly Simon says???", "Torkey is for Cristmas, right?", "Snow is white, roses are red.",
"Wanna build snowman?", "Jingle bells...\nJingle bells...", "What a wonderful world!",
"Oh. Wait!", "Mmm... Milk + Cookies = Love", lambda: f"{9999 - time.time()} sec intil the New Year!", "Is anyone reading this?",
"Wanna make a snow angel?", "Piece of coal isn't a bad option", "Meet ya next year.", "Crist is not X!",
"I feel the presence.", "Snowflake for breakfast!", "What a snowy day, huh!", "Don't forget to make a present!",
"Be kind!", "Enjoy with joy", "Enjoy with Joe!", "Enjoy!", "Not bad.", "Cook more!", "Cats or dogs?",
"Have you ever seen an elf?", "This atmosphere!", "Simon didn't say hi. He is rude.", "I'll beat you in snowball 1v1",
"It took forever to write.", "Decorate your house, pleeease...", "Tired of waiting.", "THE HOLIDAYS SOON!!!",
"Winter is the best!", "I was born in December", "Snoflake scientists discover", "Look at the window & admire!",
"Precious moon", "Harder. Better.\nFaster. Stronger", "Ginger house...\nGinger house...", "Ginger all the way.", "Gluten free milk only",
"Look! I found him!", "Dragons!! Wait..\nWrong place.", "This present is sus!", "Snoflake for dinner!", "Close the window! It's cold here.",
"I am cold! These cokies are hot.", "Damn, Wrong script...", "Totaly not scripted.", "GPT-3 is in my heart.", "RIGIK93 used to be here!", "Exactly at midnight.",
"Look inside of me!", "There is a chimney! I'm burning", "Elon Nice\nHawk! Work!", "It's warm inside.", "Come in!\nTake a seat!", "Not cardboard!\nPure gingerbread",
"Space purr...", "Feed me!\nMore logs!", "I can serve a cup of tea.", "Haha. Big BEN", "I'm Alfred by the way.", "I don't have Twitter yet.", "SmartTV only.",
"I want a freind. Buy me Alexa!", "My chimney is smoking.", "Have been built up carefully.", "My father is just a skyscraper!", "Put the snow away from my porch!"]

def main():
    while True:
        splash = splashes[random.randint(0, len(splashes)-1)]
        if (type(splash) != type(" ")):
            splash = splash()
        if (len(splash) > 32):
            print(f"Warn! This is more than 32 chars: \"{splash=}\"")
        lcd.lcd_display_string_pos(splash, 1, 0)
        time.sleep(30)
        lcd.lcd_clear()

if __name__ == '__main__':
    main()
